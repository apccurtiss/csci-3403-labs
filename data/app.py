from ast import For
from dataclasses import dataclass
from hashlib import md5
from http.client import UnimplementedFileMode
import logging
import sys
from typing import Any, Dict, List, Optional, Set, Tuple

import click
from flask import abort, Blueprint, Flask, redirect, render_template, request, session, url_for
from flask_login import current_user, login_required, login_user, LoginManager # type: ignore
from flask_wtf import FlaskForm # type: ignore
from werkzeug.datastructures import MultiDict # type: ignore
from werkzeug.exceptions import BadRequest, Forbidden, InternalServerError, NotFound # type: ignore
from werkzeug.wrappers import Response # type: ignore
from wtforms import PasswordField, SelectField, StringField, TextAreaField # type: ignore
from wtforms.validators import DataRequired, Length # type: ignore
from wtforms.widgets import TextArea # type: ignore
# from flask_sqlalchemy import SQLAlchemy


from lib.python.database import Database
from lib.python.documents import Document
from lib.python.permissions import Permission
from lib.python.users import User

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, encoding='utf-8', level=logging.INFO)
for logger_name in [
    'werkzeug',
    'engineio.server'
]:
    logging.getLogger(logger_name).disabled = True

app = Flask(__name__)
app.config['SECRET_KEY'] = 'qwer'

login_manager = LoginManager(app)
login_manager.login_view = 'core.login'

DB = Database()

@app.context_processor
def inject_debug_info() -> Dict[str, Any]:
    return {
        'build_query': build_query,
    }

@login_manager.user_loader
def load_user(username: str) -> Optional[User]:
    return DB.get_user(username)

core = Blueprint('core', __name__)

@app.errorhandler(InternalServerError)
def internal_server_error(_: InternalServerError) -> Tuple[str, int]:
    return (render_template('error.html', username=current_user.username, message='Server error!'), 500)

@app.errorhandler(NotFound)
def file_not_found(e: NotFound) -> Tuple[str, int]:
    return (render_template('error.html', username=current_user.username, message=e.description), 404)

@app.errorhandler(BadRequest)
def bad_request(e: BadRequest) -> Tuple[str, int]:
    return (render_template('error.html', username=current_user.username, message=e.description), 400)

@app.errorhandler(Forbidden)
def forbidden(e: Forbidden) -> Tuple[str, int]:
    return (render_template('error.html', username=current_user.username, message=e.description), 403)

class DifficultyForm(FlaskForm):
    question_level = SelectField('Label', choices=[(1,'Question 1a'), (2, 'Question 1b'), (3, 'Question 1c')])

@core.route('/', methods=['GET', 'POST'])
@login_required
def index() -> str:
    form = DifficultyForm()

    if form.validate_on_submit():
        question_level = form.question_level.data
        logger.info('User {} changed question: {}'.format(current_user, question_level))
        current_user.question_level = int(question_level)

    documents = DB.get_document_store(current_user).get_all_documents()
    return render_template('index.html', user=current_user, documents=documents, form=form)

def get_last_parameter(param: str) -> Optional[str]:
    list: List[str] = request.args.getlist(param)

    if not list:
        return None
    return list[-1]

class CreateForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired(), Length(min=1, max=128)])
    content = StringField('Content', widget=TextArea(), validators=[DataRequired(), Length(max=2048)])

@core.route('/create', methods=['GET', 'POST'])
@login_required
def create() -> Response|str:
    form = CreateForm()

    if form.validate_on_submit():
        DB.get_document_store(current_user).new_document(form.title.data, form.content.data, current_user, set())
        return redirect(url_for('core.index'))

    return render_template('create.html',
        user=current_user,
        form=form)

def build_query(user: User, action: str) -> str:
    query = 'user={}&action={}'.format(user.username, action)

    if current_user.question_level == 1:
        return query
    elif current_user.question_level == 2:
        return '{}&md5_validation={}'.format(query, md5(query.encode()).hexdigest())
    elif current_user.question_level == 3:
        return '{}&md5_validation={}'.format(query, md5((user.password + query).encode()).hexdigest())
    else:
        raise ValueError('User has unknown question level: {}'.format(current_user.question_level))

def validate_query_string(user: User) -> None:
    validation_key = 'md5_validation'
    query_string = request.query_string.decode()

    command, *_ = query_string.rsplit('&{}'.format(validation_key), 1)
    validator = get_last_parameter(validation_key)

    if current_user.question_level == 2:
        if validator != md5(command.encode()).hexdigest():
            raise Forbidden('Incorrect validator for command: {}'.format(command))

    if current_user.question_level == 3:
        if validator != md5((user.password + command).encode()).hexdigest():
            raise Forbidden('Incorrect validator for command: {}'.format(command))

class EditForm(FlaskForm):
    content = TextAreaField(
            'Content',
            validators=[DataRequired(), Length(max=2048)])

@core.route('/document/<document_id>', methods=['GET', 'POST'])
@login_required
def document(document_id: str) -> Response|str|Tuple[str, int]:
    document = DB.get_document_store(current_user).get_document(document_id)
    if not document:
        raise NotFound('Document not found.')

    action = get_last_parameter('action')
    if not action:
        raise BadRequest('No action specified.')

    username = get_last_parameter('user')
    if not username:
        raise BadRequest('No user specified.')

    user = DB.get_user(username)
    if not user:
        raise BadRequest('No user found with username: {}'.format(username))
    
    validate_query_string(user)

    edit_permitted = document.has_permission(user, Permission.Edit)
    delete_permitted = document.has_permission(user, Permission.Delete)

    if action == 'view':
        if not document.deleted:
            return render_template('view.html',
                document=document,
                user=user,
                edit_permitted=edit_permitted,
                delete_permitted=delete_permitted)
        else:
            return render_template('deleted.html',
                document=document,
                user=user)

    elif action == 'edit':
        if not edit_permitted:
            raise Forbidden('You do not have permission to edit this document.')
        form = EditForm()

        if form.validate_on_submit():
            document.content = form.content.data
            logging.info('Content of {} updated to be {}'.format(document.title, document.content))
            return redirect(url_for('core.index'))

        return render_template('edit.html',
            document=document,
            user=user,
            form=form)

    elif action == 'delete':
        if not delete_permitted:
            raise Forbidden('You do not have permission to delete this document.')
        document.deleted = True
        logging.info('{} deleted'.format(document.title))
        return redirect(url_for('core.index'))
    else:
        raise BadRequest('Unknown action: {} not in ["view", "edit", "delete"]'.format(action))

class LoginForm(FlaskForm):
    username = StringField('Username', [Length(min=1, max=25)])
    password = PasswordField('Password', [Length(min=8, max=64)])

@core.route('/login', methods=['GET', 'POST'])
def login() -> Response:
    user = DB.new_user('debug', 'debugpass')
    login_user(user)
    next = request.args.get('next')
    return redirect(next or url_for('core.index'))
    form = LoginForm()

    if form.validate_on_submit():
        user = DB.new_user(form.username.data)
        login_user(user)

        next = request.args.get('next')
        return redirect(next or url_for('core.index'))

    return render_template('login.html', form=form)

@click.command()
@click.option('--debug', is_flag=True)
def main(debug: bool) -> None:
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)

    logger.info('Starting up...')
    logger.info(logger.level)
    
    app.register_blueprint(core)
    app.run(debug=debug)

if __name__ == '__main__':
    main()