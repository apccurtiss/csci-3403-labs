from enum import Enum

class Permission(Enum):
    Edit = 'edit'
    Delete = 'delete'