from ctypes import Union
from datetime import datetime
import logging
import random
import string
import time
from typing import Dict, List, Optional, Set
from uuid import UUID

from lib.python.documents import Document
from lib.python.permissions import Permission
from lib.python.users import User

logger = logging.getLogger(__name__)


class DocumentStore:
    _random: random.Random
    _documents: Dict[str, Document] = {}

    def __init__(self, username: str):
        self._random = random.Random(1)

        self.new_document('First post', 'Just setting up my doczone. The site just went live a second ago!', 'admin', set())
        self.new_document('Grocery list', 'Eggs\nCheese\nBread\nCake?', 'alice', {Permission.Edit})
        self.new_document('Temp', 'Test post, feel free to delete if you want', 'bob', {Permission.Delete})

    def new_document(
        self,
        title: str,
        content: str,
        owner: str,
        permissions: Set[Permission]) -> Document:

        id = ''.join(self._random.choices(string.ascii_letters, k=32))
        created = datetime.now()

        doc = Document(id, title, content, owner, created, permissions)
        self._documents[id] = doc
        return doc
    
    def get_document(self, document_id: str) -> Optional[Document]:
        return self._documents.get(document_id)

    def get_all_documents(self) -> List[Document]:
        return [doc for doc in self._documents.values() if not doc.deleted]


class Database:
    _users: Dict[str, User] = {
        'admin': User('admin', 'cheese'),
        'alice': User('alice', '29nv0bv0palknv'),
        'bob': User('bob', '10hasdpnl49asdf'),
    }
    _document_stores: Dict[str, DocumentStore] = {}

    def new_user(self, username: str, password: str) -> User:
        logger.info('Creating new user: {}'.format(username))

        self._users[username] = User(username, password)
        self._document_stores[username] = DocumentStore(username)
        return self._users[username]
    
    def get_user(self, username: str) -> Optional[User]:
        return self._users.get(username)

    def get_document_store(self, user: User|str) -> DocumentStore:
        return self._document_stores[str(user)]