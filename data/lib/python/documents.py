from dataclasses import dataclass
from datetime import datetime
from random import random
from typing import Dict, Set

from lib.python.permissions import Permission
from lib.python.users import User

@dataclass
class Document:
    id: str
    title: str
    content: str
    owner: str
    created: datetime
    permissions: Set[Permission]

    deleted: bool = False

    def has_permission(self, user: User, permission: Permission) -> bool:
        return (user.username == self.owner) or (permission in self.permissions)

    def __hash__(self) -> int:
        return hash(self.id)