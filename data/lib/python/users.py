from dataclasses import dataclass
import uuid

from flask_login import UserMixin # type: ignore


@dataclass
class User(UserMixin):
    username: str
    password: str

    question_level: int = 1

    def get_id(self) -> str:
        return self.username

    def __str__(self) -> str:
        return self.username

    def __hash__(self) -> int:
        return hash(self.username)